import pigpio
from parser import Parser

class Aegir(object):

	
	def __init__(self, file, host = 'localhost',
				 base = 'LIRC', port=8888,
				 pin = 23, transistor = True, show_errors = False):
		self.pi = pigpio.pi(host, port, show_errors)
		self.pin = pin
		self.transistor = transistor
		parse = Parser(base, file)
		self.__rAn(parse.out(), self.transistor)
		
	def __getitem__(self, key):
		return self.pi.wave_send_once(self.__dict__[key])
	
	def __rAn(self, raw, transistor):
		try:
			for i in raw:
				waves = []
				for n in range(int(len(raw[i])/2))
					if transistor:
						waves.append(pigpio.pulse(1<<self.pin,0,raw[i].pop(0)))
						waves.append(pigpio.pulse(0,1<<self.pin,raw[i].pop(0)))
					else:
						print("[FATAL] >>> Sans-transistor logic does not exist. Nothing will happen now.")
						exit()
				self.pi.wave_add_generic(waves)
				self.__dict__[i] = self.pi.wave_create()
		except AttributeError:
			print("[FATAL] >>> pigpiod daemon must be running on target device.")
			exit()			
				
	def show(self, key="", show_data = False):
		if key == "":
			if not show_data:
				for i in self.__dict__:
					print(i)
			else:
				for i in self.__dict__:
					print(i + ":")
					print(self.__dict__[i])
		else:
			print(self.__dict__[key])
				
