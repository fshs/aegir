class Parser(object):
	
	FLAGS = ['RC5','RC6','RCMM','SHIFT_ENC','REVERSE',
		 'NO_HEAD_REP','NO_FOOT_REP','CONST_LENGTH',
		 'RAW_CODES','REPEAT_HEADER']
	
	TESTING = False
	
	def __init__(self, base, file):
		self.file = file
		self.MODE = self.__determine_mode()
		self.__read(file)
			
		
	
	def __determine_mode(self):
		return 'DEFAULT'
	
	def __read(self, file):
		with open(file, 'r') as f:
			content = f.read()
			f.close()
		content = self.__sanitize(content)
		content = self.__rationalize(content)
		self.__main(*content)
		
		
	def __sanitize(self, s):
		x = []
		for line in s.splitlines():
			if len(line)>0:	
				if line.strip()[0] != ('#'): x.append(line)
		s = ''
		for line in x:
			if '#' in line: line = line.split('#')[0]
			line = line.strip()
			s = s + line + '\n'
		return s
	def __rationalize(self, s):
		s = s.split('begin') #split into candidate blocks
		for block in s:
			if len(block)>0: #discard empty candidates
				#test survivors for leading keywords
				if block.strip().split()[0] == 'remote':
					config = block.splitlines()[1:]
				if any(x == block.strip().split()[0] for x in ('raw_codes','codes')): 
					codes = block.splitlines()[1:]
		for i in range(len(codes)): #locate the end of the code block
			if any(x == codes[i] for x in ('end codes','end raw_codes')): t = i
		codes = codes[:t] #discard trailing lines
		#right now both config and codes are lists of lines
		
		config_dict = {}
		for conf in config:
			n = conf.split() #split on whitespace
			if len(n) == 2: #flags is in here with most everything else
				config_dict.update({n[0].strip() : n[1].strip()})
			elif len(n) == 3: #header, zero, one, two, three, post, etc.
				x = [int(n[1].strip()),int(n[2].strip())]
				config_dict.update({n[0].strip() : x})
			else:	print("UNHANDLED CASES")
				
		for key in config_dict:  # final handling
			if '|' in config_dict[key]:
				l = config_dict[key].split('|')
				for i in l: i = i.strip()
				config_dict.update({key : l})
			#handle integers
			if key in ['manual_sort','supress_repeats','bits','eps','aeps','ptrail',
					   'plead','pre_data_bits','post_data_bits','gap','repeat_gap',
					   'min_repeat','frequency','duty_cycle']:
				config_dict[key] = int(config_dict[key])
			if key in ['header','three','two','one','zero','foot','repeat',
					   'pre','post']:
				for i in range (len(config_dict[key])): 
					config_dict[key][i] = int(config_dict[key][i])
					#config_dict[key][i]=int(config_dict[key][i])
			#handle flags
		return(config_dict, codes)
	
	def __realize(self, config, codes):
		#print("[AEGIR] >>> Configuration dictionary constructed. Initializing parser...")
		__main(__rationalize) 

	
	def out(self):
		return self.r
		
	def __main(self, config_dict, codes):
		
		self.__dict__.update(config_dict)
		self.c = codes #a string with line breaks
		self.r = {} # Raw, Return, whatever. we will pass this to rAn
		self.__flags()
		if self.RAW_CODES: self.__raw_config()
		else: self.__hex_config()
			
	def __flags(self):
		for f in Parser.FLAGS: self.__dict__.update({f : False})
		for f in self.flags: self.__dict__.update({f : True})
		del self.flags
			
	def __raw_config(self):
		##make lines back into a string so that we can split it at 'name'
		s = ""
		for line in self.c:
			s = s + line + '\n'
		###k now split it
		self.c = s.split('name')
		###get code names and contents
		for m in self.c:
			if len(m) > 1: #just to be safe
				m = m.strip().split()
				n = m.pop(0).strip()
				for x in range(len(m)):
					m[x] = int(m[x].strip())
				self.r.update({n:m})
		del self.c
		self.__handle_gap()
			
	def __hex_config(self):
		
		for l in self.c: #for each line in the unbroken code block string
			ws = l.split()
			for w in range(len(ws)): ws[w] = ws[w].strip()
			self.r.update({ws[0] : ws[1]})
		
		where = self.__stray_pulses('L') #We need to do this early in case plead appends to data
		for key in self.r: 
			#r is now the 'raw' or 'return' dictionary, as you please
			self.r[key] = self.__pulser(self.__binary(self.r[key],self.bits))
		if self.__stray_pulses('L') == 'data':
			"""
			the spec, if not the real world, allows for a potential scenario where plead exists but
			pre, and pre_data do not.  We check for this, and if it is the case, we sum the plead
			duration to the first ON duration in each data block in order to maintain the ON/OFF
			sanity that rAn requires
			""" 
			for key in self.r: self.r[key][0] = self.r[key][0]+self.plead
		L = self.__left_pulses() #Prepare left raw block
		R = self.__right_pulses() #Prepare right raw block
		for key in self.r:
			self.__assemble(L, R, key)
		self.__handle_gap()
		
	def __handle_gap(self):
		if self.CONST_LENGTH:
			for key in self.r:
				self.r[key].append(int(self.gap)-sum(x for x in self.r[key]))
		else:
			for key in self.r:
				self.r[key].append(self.gap)
	
	def __binary(self, data, bits):
		return bin(int(data, 16))[2:].zfill(bits)
	
	def __pulser(self, binary):
		t = []
		for b in binary:
			if int(b) == 0:
				for i in self.zero:
					t.append(int(i))
			if int(b) == 1:
				for i in self.one:
					t.append(int(i))
		return t
				
	def __left_pulses(self, mode = 'default'): #Constructs the left pulse block as a list
		pulses = []
		if mode == 'default': #Only this mode for now.  Sans-transistor or non-lirc mode potential?
			if hasattr(self, 'header'):
				for p in self.header: pulses.append(p)
			if hasattr(self, 'plead'): where = self.__stray_pulses('L')
			else: where =""
			if hasattr(self, 'pre_data'):
				pd = self.__pulser(self.__binary(self.pre_data, int(self.pre_data_bits)))
				if where == 'pre_data': pd[0] = pd[0]+int(self.plead)
				for p in pd: pulses.append(p)
			if hasattr(self, 'pre'):
				if where == 'pre': self.pre[0] = self.pre[0]+int(self.plead)
				for p in self.pre: pulses.append(p)
			return pulses
		
	def __right_pulses(self, mode = 'default'): #Constructs the right pulse block as a list
		pulses = []
		if mode == 'default':
			if hasattr(self, 'post'):
				for p in self.post:
					pulses.append(p)
			if hasattr(self, 'post_data'):
				ps = self.__pulser(self.__binary(self.post_data,int(self.post_data_bits)))
				for p in ps:
					pulses.append(p)
			if hasattr(self, 'ptrail'): where = self.__stray_pulses('R')
			if hasattr(self, 'foot'):
				if where == 'foot': self.foot[0] = self.foot[0] + self.ptrail
				else: pulses.append(self.ptrail)
				for p in self.foot: pulses.append(p)
			return pulses
	
	def __stray_pulses(self, a):
			#the LIRC conf spec defines plead and ptrail as only pulses, with no trailing off-time.
			#rAn must receive alternating on-offs. We check to see what exists to the right of us
			#so that we can append the stray pulse duration to that member's first pulse..
			#Remember that the data block necessarily exists. ##WRITE TO THE SPEC
			if a=='L': #we are in the left block
				if 'pre_data' in self.__dict__:
					return 'pre_data'
				elif 'pre' in self.__dict__:
					return 'pre'
				else:
					return 'data'
			elif a=='R': #we are in the right block
				if 'foot' in self.__dict__: return 'foot'#we know that there is a gap				
		
	def __assemble(self, ls, rs, k): 
		#Replace the hex values in self.r[k] with a list of raw pulse
		#times from left to right.  rAn MUST receive definitions of an 
		#even length that begin with on and end with off.
		full_raw = [] # This will become the value of self.r[k]
		
		for l in ls: full_raw.append(l) #Add the left pulse block to the assembly
		for c in range(len(self.r[k])): full_raw.append(self.r[k][c]) #Add central block from dictionary r 
		for r in rs: full_raw.append(r) #Add the right block
		self.r[k] = full_raw # Modify the dictionary value

				
			
